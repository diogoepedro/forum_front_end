import React, {useEffect, useState} from "react";
import axios from "axios"

function LoginForm(){

    const[users, setUsers] = useState([]);
    const[userName, setUserName] = useState('');
    const[password, setPassword] = useState('');

    const client = axios.create({
        baseURL: "https://localhost:7056/api/Users" 
      });

    useEffect(() => {
        client.get().then((response) =>{
            setUsers(response.data);
            console.log(users);
        }).catch(error => console.error(`Error: ${error}`));
    }, []);

    function onSubmitHandler(){
        console.log( users)
    }

  
    return <div>
        <form onSubmit={onSubmitHandler}>
            <div>
                <label>UserName</label>
                <input type='text' id = "username-field" onChange={event => setUserName(event.target.value)}/>
                <label>Password</label>
                <input type= 'password' id = "password-field" onChange={event => setPassword(event.target.value)}/>
            </div>
            <button>Login</button> 
        </form>
    </div>
}

export default LoginForm;