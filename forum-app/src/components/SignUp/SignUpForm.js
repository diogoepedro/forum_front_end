import React, {useState} from "react";
import axios from "axios";

function SignUpForm(){

    const[userName, setUserName] = useState('');
    const[email, setEmail] = useState('');
    const[password, setPassword] = useState('');

    


    function addUser(userName,email,password){

        axios.post("https://localhost:7056/api/Users",{ 
            username: userName,
            email: email,
            password: password,
            roleId: 3
        }).catch(error => console.error(`Error: ${error}`));
      }


    function onSubmitHandler(event){
        event.preventDefault();
        addUser()
    }

    return <div>
        <form onSubmit={onSubmitHandler}>
        <div>
            <label>Email</label>
            <input type = 'text' id = 'email-signup-field' onChange={event => setEmail(event.target.value)} />
            <label>Username</label>
            <input type = 'text' id = 'username-signup-field' onChange={event => setUserName(event.target.value)}/>
            <label>Password</label>
            <input type = 'password' id = 'password-signup-field' onChange={event => setPassword(event.target.value)}/>
        </div>
        <button>SignUp</button>
    </form>
    </div>
}

export default SignUpForm;