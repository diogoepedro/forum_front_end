import React from "react";
import {Link} from "react-router-dom"

function LinkTree(){
    return <div>
        <ul>
        <h1>SignUP</h1>
        <li>  
        <Link to = "/signup">SignUP</Link>
        </li>
        <h1>Login</h1>
        <li>
        <Link to = "/login">Login</Link>
        </li>
        <h1>Post</h1>
        <li>  
        <Link to = "/uselessreddit">Post Something</Link>
        </li>
        </ul>
        </div>
}

export default LinkTree;