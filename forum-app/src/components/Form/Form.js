import React, {useState} from "react";
import axios from "axios";

function Form(){
    const [body, setBody] = useState('');
    const [title, setTitle] = useState('');

    async function onSubmitHandler(){
        axios.post("https://localhost:7056/api/Posts",{ 
           title: title,
           body: body,
           category: 1,
           isPublic: true,
        }).catch(error => console.error(`Error: ${error}`));
        
    }


    return <form onSubmit={onSubmitHandler}>
        <div>
            <label>Title</label>
            <input text = 'text' id = 'post-title' onChange={event => setTitle(event.target.value)}/>
            <label>Post</label>
            <input type= 'text' id="post-body" onChange={event => setBody(event.target.value)}/>
        </div>
        <div>
            <button>Submit</button>
        </div>
    </form>
}
    export default Form;


