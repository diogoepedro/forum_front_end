import SignUpForm from "./components/SignUp/SignUpForm"
import LoginForm from "./components/Login/LoginForm"
import Form from "./components/Form/Form"
import LinkTree from "./components/LinkTree/LinkTree"
import {
  BrowserRouter as Router,
  Route,
  Routes,
} from "react-router-dom"; 

function App() {
  return (
    <Router>
      <LinkTree></LinkTree>
      <Routes>
        <Route path="/signup" element = {<SignUpForm/>}/>
        <Route path="/login" element = {<LoginForm/>}/>
        <Route path="/uselessreddit" element = {<Form/>}/>
      </Routes>
    </Router>
  );
}

export default App;
